use cooking_db;
go

-----------------------------------------------------------------------
/*
	Favorite ingredients of the big chefs.

	Find most used ingredients and usage in recipes that are not 'easy', 
	made by authors with >10 years experience
	5 complexity
*/
-----------------------------------------------------------------------

CREATE OR ALTER PROCEDURE cooking_schema.find_favorite_ingredients
AS
	select
		Ingredients.id,
		Ingredients.title,
		count(ingredients.id) as usage
		from 	
			cooking_schema.Recipes Recipes 
			JOIN cooking_schema.Recipe_Ingredient Recipe_Ingredient -- +1 point complexity
				on Recipes.id = Recipe_Ingredient.recipe_id
			JOIN cooking_schema.Ingredients Ingredients -- +1
				on Recipe_Ingredient.ingredient_id = Ingredients.id
			JOIN cooking_schema.Authors Authors -- +1
				on Recipes.author_id = Authors.id
		where -- +1
			Authors.experience > 10
			AND Recipes.difficulty != 'Easy'
		group by -- +1
			ingredients.id, ingredients.title
		order by
			usage desc
GO

-----------------------------------------------------------------------
/*
	Ambitious users, still seeking the perfect dessert.

	Find users with biggest total learning scores (and their scores), 
	who are not also authors, 
	that gave at least 3 unfavorable (<=3 stars) reviews to desserts

	We define learning score by summing up the learning interest skill score

	Complexity: 8
*/
-----------------------------------------------------------------------

CREATE OR ALTER PROCEDURE cooking_schema.find_ambitious_users
AS
	select 
		Learning_Users.id,
		Learning_Users.fullname,
		Learning_Users.total_score
		from (
			select 
				 Users.id as id,
				 Users.fullname as fullname,
				 SUM(Learning_Interest.skill_score) as total_score
			from 
				cooking_schema.Users Users 
				JOIN cooking_schema.User_Learning_Interest User_Learning_Interest -- +1
					on Users.id = User_Learning_Interest.learning_user_id
				JOIN cooking_schema.Learning_Interests Learning_Interest -- +1
					on User_Learning_Interest.learning_interest_id = Learning_Interest.id
			WHERE
				Users.fullname NOT IN (
					select fullname from cooking_schema.Authors
				)
			GROUP BY -- +1
				Users.id, 
				Users.fullname
		) as Learning_Users
		JOIN cooking_schema.Reviews Reviews on Learning_Users.id = Reviews.reviewing_user_id -- +1
		JOIN cooking_schema.Recipes on Reviews.recipe_id = Recipes.id -- +1
	
		WHERE 
			Recipes.category = 'Dessert' -- +1
			AND Reviews.stars <= 3

		GROUP BY  -- +1
			Learning_Users.id,
			Learning_Users.fullname,
			Learning_Users.total_score

		-- filtering users which gave at least 4 reviews (to desserts) 
		HAVING -- +1
			COUNT(recipe_id) >= 3

		ORDER BY Learning_Users.total_score DESC
GO


-----------------------------------------------------------------------
/*
	Doable, fast, reviewed recipes of authors that use secure email providers, for users with cups

	Find recipes with least average total time (prep + cook) per portion
	Filter by those that 
	- are not Advanced
	- contain ingredients measured in cups
	- are made by authors that use gmail or hotmail
	- reviewed at least 14 times

	Complexity: 9
*/

CREATE OR ALTER PROCEDURE cooking_schema.find_doable_recipes
AS
	SELECT
		Cup_Recipes.id,
		Cup_Recipes.title,
		Cup_Recipes.average_time_per_portion,
		Authors.email,
		count(Reviews.recipe_id) as no_reviews
		FROM(
			SELECT
				Recipes.id,
				Recipes.title,
				cast(
					round( (Recipes.prep_time + Recipes.cook_time) / (Recipes.serving * 1.0) , 2)
					as numeric(36,2)
				) as average_time_per_portion
			FROM
				cooking_schema.Recipes 
				JOIN cooking_schema.Recipe_Ingredient Recipe_Ingredient on Recipes.id = Recipe_Ingredient.recipe_id -- +1
				JOIN cooking_schema.Ingredients Ingredients on Recipe_Ingredient.ingredient_id = Ingredients.id -- +1
			WHERE 
				Ingredients.measurement = 'Cup' -- +1
				AND Recipes.difficulty != 'Advanced'
			GROUP BY -- +1
				Recipes.id,
				Recipes.title,
				cast(
					round( (Recipes.prep_time + Recipes.cook_time) / (Recipes.serving * 1.0) , 2)
					as numeric(36,2)
				)
		) as Cup_Recipes
		-- one author per recipe, no need for grouping
		JOIN cooking_schema.Authors Authors on Cup_Recipes.id = Authors.id -- +1
		JOIN cooking_schema.Reviews ON Cup_Recipes.id = Reviews.recipe_id -- +1

		WHERE email LIKE '%@gmail.com' -- +1
			or email LIKE '%@hotmail.com'
	
		GROUP BY -- +1
			Cup_Recipes.id,
			Cup_Recipes.title,
			Cup_Recipes.average_time_per_portion,
			Authors.email

		HAVING -- +1
			count(Reviews.recipe_id) >= 14
		ORDER BY
			Cup_Recipes.average_time_per_portion
GO

CREATE OR ALTER PROCEDURE cooking_schema.do_reports
AS
	exec cooking_schema.find_favorite_ingredients
	exec cooking_schema.find_ambitious_users
	exec cooking_schema.find_doable_recipes
GO
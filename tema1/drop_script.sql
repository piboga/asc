use cooking_db;
go

drop procedure if exists cooking_schema.do_reports
drop procedure if exists cooking_schema.find_favorite_ingredients
drop procedure if exists cooking_schema.find_ambitious_users
drop procedure if exists cooking_schema.find_doable_recipes

drop table if exists cooking_schema.Reviews
drop table if exists cooking_schema.User_Learning_Interest
drop table if exists cooking_schema.Users
drop table if exists cooking_schema.Recipe_Ingredient
drop table if exists cooking_schema.Recipes
drop table if exists cooking_schema.Authors
drop table if exists cooking_schema.Ingredients
drop table if exists cooking_schema.Learning_Interests
drop schema if exists cooking_schema;

go
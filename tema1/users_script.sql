use cooking_db;
go

CREATE TABLE cooking_schema.Users (
    id INT PRIMARY KEY IDENTITY(1,1), -- auto-incrementing primary key
	fullname VARCHAR(255) NOT NULL,
    nickname VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL CHECK (email LIKE '%@%.%'),
);

INSERT INTO cooking_schema.Users (
	fullname, 
	nickname,
	email
)
VALUES 
    ('Jane Doe', 'janedoe', 'janedoe@gmail.com'), 
    ('Emily Johnson', 'emilyj', 'emilyj@hotmail.com'),
    ('Evan Gated', 'evangated', 'evangated@yahoo.com'), 
    ('Samantha Lee', 'samanthal', 'samanthal@yahoo.com'),
    ('John Smith', 'johnsmith', 'johnsmith@gmail.com'),
    ('Michael Davis', 'mdavis', 'mdavis@gmail.com'),
    ('Cameron Ezekiel', 'ecameron', 'ecameron@gmail.com'),
    ('Madison Mitchell', 'mmitchell', 'mmitchell@hotmail.com'),
    ('Avery Lewis', 'alewis', 'alewis@yahoo.com'),
    ('Elizabeth Green', 'egreen', 'egreen@gmail.com'),
    ('Bryan Johnson', 'bryanj', 'bryanj@gmail.com'),
    ('Amanda Smith', 'amandas', 'amandas@hotmail.com'),
    ('Chloe Wilson', 'chloew', 'chloew@yahoo.com'),
    ('Jacob Davis', 'jacobd', 'jacobd@gmail.com'),
    ('Emily Thompson', 'emilyt', 'emilyt@hotmail.com'),
    ('Nathan Anderson', 'nathan', 'nathan@yahoo.com'),
    ('Madison White', 'madison', 'madison@gmail.com'),
    ('Hannah Anderson', 'hannah', 'hannah@hotmail.com'),
    ('Hailey Williams', 'yeliah', 'hwilliams@paramore.com'),
	('Matty Healy', 'truman', 'truman@1975.com')


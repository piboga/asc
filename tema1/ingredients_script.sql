use cooking_db;
go

CREATE TABLE cooking_schema.Ingredients (
    id INT PRIMARY KEY IDENTITY(1,1),
    title VARCHAR(255) NOT NULL,

    measurement VARCHAR(255) NOT NULL CHECK (
		measurement IN (
			'pound', 'can', 'stick', 'cup', 'quart', 'teaspoon', 'tablespoon', 
			'clove', 'large', 'pack'
		)
	),

    category VARCHAR(255) NOT NULL CHECK (
		category IN (
			'spice', 'pasta', 'vegetable', 'baking', 'sweetener', 'dairy', 
			'cheese', 'water', 'oil', 'meat', 'grain' 
		)
	)
);

go

INSERT INTO cooking_schema.Ingredients (title, measurement, category)
VALUES 
	('ground beef', 'pound', 'meat'), -- 1 
	('spaghetti', 'pound', 'pasta'),  -- 2 
	('diced tomatoes', 'can', 'vegetable'), -- 3 
	('semi-sweet chocolate chips', 'cup', 'baking'), -- 4 
	('butter', 'stick', 'dairy'),  -- 5
	('sugar', 'cup', 'sweetener'), -- 6 
	('eggs', 'large', 'dairy'),  -- 7 
	('all-purpose flour', 'cup', 'baking'), -- 8 
	('baking powder', 'teaspoon', 'baking'), -- 9
	('milk', 'cup', 'dairy'), -- 10
	('cornmeal', 'cup', 'grain'), -- 11
	('water', 'quart', 'water'),  --  12
	('salt', 'teaspoon', 'spice'), -- 13
	('cheddar cheese', 'cup', 'cheese'), -- 14 
	('sour cream', 'cup', 'dairy'), -- 15
	('vanilla extract', 'teaspoon', 'spice'), -- 16 
    ('heavy cream', 'cup', 'dairy'), -- 17
	('ground pork', 'pound', 'meat'), -- 18 
	('ricotta cheese', 'pound', 'dairy'), -- 19 
	('mozzarella cheese', 'pound', 'cheese'), -- 20
	('parmesan cheese', 'cup', 'cheese'), -- 21
	('onion', 'large', 'vegetable'), -- 22
	('garlic', 'clove', 'spice'), -- 23
	('oregano', 'teaspoon', 'spice'), -- 24
	('basil', 'teaspoon', 'spice'), -- 25
	('nutmeg', 'teaspoon', 'spice'), -- 26
	('olive oil', 'tablespoon', 'oil'), -- 27
	('red pepper flakes', 'teaspoon', 'spice'), -- 28
	('no-boil lasagna noodles', 'pack', 'pasta'), -- 29
	('sunflower oil', 'cup', 'oil'); -- 30
go
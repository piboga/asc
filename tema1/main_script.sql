use cooking_db;
go


/*
	modify with your path. toggle "query/SQL mode" in the menu.
*/
:setvar SCRIPT_PATH "C:\\BD2_PROIECT\\scripts"

:r $(SCRIPT_PATH)"\\drop_script.sql"

CREATE SCHEMA cooking_schema;
go

:r $(SCRIPT_PATH)"\\authors_script.sql"
:r $(SCRIPT_PATH)"\\recipes_script.sql"
:r $(SCRIPT_PATH)"\\ingredients_script.sql"
:r $(SCRIPT_PATH)"\\recipe_ingredient_script.sql"
:r $(SCRIPT_PATH)"\\users_script.sql"
:r $(SCRIPT_PATH)"\\learning_interests_script.sql"
:r $(SCRIPT_PATH)"\\user_learning_interest_script.sql"
:r $(SCRIPT_PATH)"\\reviews_script.sql"

:r $(SCRIPT_PATH)"\\reports_script.sql"

exec cooking_schema.do_reports

go

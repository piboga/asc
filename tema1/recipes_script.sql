use cooking_db;
go

CREATE TABLE cooking_schema.Recipes (
    id INT PRIMARY KEY IDENTITY(1,1), -- auto-incrementing primary key
    title VARCHAR(255) NOT NULL,
    details TEXT NOT NULL,
    instructions TEXT NOT NULL,
    category VARCHAR(255) NOT NULL CHECK (
		category in ('Main dish', 'Dessert')
	),
    difficulty VARCHAR(255) NOT NULL CHECK (
		difficulty in ('Easy', 'Intermediate', 'Advanced')
	),
    serving INT NOT NULL CHECK (serving > 0),
    prep_time INT NOT NULL CHECK (prep_time > 0),
    cook_time INT NOT NULL CHECK (cook_time > 0),
    author_id INT FOREIGN KEY REFERENCES cooking_schema.Authors(id)
);
go

INSERT INTO cooking_schema.Recipes 
(
    title, 
    details, 
    instructions, 
    category, 
    difficulty, 
    serving, 
    prep_time, 
    cook_time, 
	author_id
)
VALUES 
	(
		'Spaghetti Bolognese', 
		'A classic Italian dish made with ground beef, tomatoes, and spaghetti.', 
		'1) Cook spaghetti according to package instructions. 
		2) In a separate pan, brown ground beef. 
		3) Add diced tomatoes and seasonings to the pan and let simmer for 15 minutes. 
		4) Serve the beef mixture over the cooked spaghetti.', 
		'Main Dish', 
		'Easy', 
		4, 
		20, 
		30, 
		1
	),
	(
		'Lava Cake', 
		'A decadent chocolate cake with a warm, gooey center.', 
		'1) Preheat oven to 425 degrees F. 
		2) In a mixing bowl, combine flour, sugar, cocoa powder, and baking powder. 
		3) In a separate bowl, mix together eggs, milk, and melted chocolate. 
		4) Gradually add the wet ingredients to the dry ingredients and mix well. 
		5) Pour batter into ramekins and place on a baking sheet. 
		6) Bake for 12-15 minutes, or until the edges are firm and the center is still gooey. 
		7) Serve warm with a scoop of vanilla ice cream.', 
		'Dessert', 
		'Intermediate', 
		4, 
		20, 
		15, 
		6
	),
	(
		'Polenta with Cheese and Sour Cream', 
		'A traditional Romanian dish made with polenta, cheese, and sour cream.', 
		'1) Bring water and salt to a boil in a large pot. 
		2) Slowly pour in the cornmeal, stirring constantly to prevent lumps. 
		3) Reduce heat and continue to stir for about 20 minutes, or until the polenta is thick and creamy. 
		4) Remove from heat and stir in grated cheese and sour cream. 
		5) Serve warm and enjoy.', 
		'Main Dish', 
		'Easy', 
		4, 
		10, 
		20, 
		4
	),
	(
		'Cheesy Beef and Tomato Casserole', 'A delicious and hearty casserole dish made with ground beef, diced tomatoes, and cheddar cheese.', 
		'1) Preheat oven to 375 degrees F (190 degrees C).
		2) In a large skillet, brown ground beef over medium-high heat.
		3) Drain any excess fat and add diced tomatoes to the skillet. Let simmer for 10 minutes.
		4) Spread the beef and tomato mixture in a 9x13 inch baking dish.
		5) Sprinkle cheddar cheese and all-purpose flour over the beef mixture.
		6) Bake in the preheated oven for 25 minutes, or until cheese is melted and bubbly.
		7) Serve with pasta, rice, or bread.',
		'Main Dish', 
		'Easy', 
		6, 
		20, 
		25,
		2
	),
	(
		'Chocolate Souffle', 
		'A classic French dessert, this chocolate souffle is light, airy, and decadently chocolatey.',
		'1) Preheat the oven to 375 degrees. Grease the souffle dish with butter. 
		2) Melt the chocolate and butter in a double boiler. 
		3) In a separate bowl, beat the egg yolks and sugar until pale and thick. 
		4) In another bowl, beat the egg whites until stiff peaks form. 
		5) Gently fold the chocolate mixture into the egg yolks, then fold in the egg whites. 
		6) Pour the mixture into the prepared dish and bake for 20-25 minutes, or until risen and set. 
		7) Serve immediately with powdered sugar and whipped cream.',
		'Dessert',
		'Advanced',
		6,
		30,
		25,
		2
	),
    (
		'Lasagna', 
		'A classic Italian dish made with layers of pasta, meat sauce, and cheese', 
		'1) Preheat the oven to 350 degrees F. Grease a 9x13 inch baking dish.
		2) Cook the ground beef in a large skillet over medium heat until browned. Drain any excess fat.
		3) Add the diced tomatoes, salt, and pepper to the skillet and bring to a simmer. Cook for 10 minutes. 
		4) Cook the spaghetti according to package instructions. Drain and set aside.
		5) In a separate bowl, mix together the ricotta cheese, eggs, and 1 cup of grated cheddar cheese.
		6) To assemble the lasagna, spread a thin layer of meat sauce in the bottom of the prepared baking dish. Layer the cooked spaghetti on top of the sauce. 
		Spread a layer of the cheese mixture over the spaghetti. Repeat the layers until all ingredients are used, ending with a layer of meat sauce on top.
		7) Sprinkle the remaining cheddar cheese on top of the lasagna.
		8) Bake the lasagna in the preheated oven for 25-30 minutes, or until the cheese is melted 
		and the lasagna is heated through.
		9) Let the lasagna cool for 10 minutes before slicing and serving.', 
		'Main Dish', 
		'Intermediate', 
		8, 
		30, 
		60, 
		2
	),
	(
		'Vanilla Cupcakes with Custard Filling and Chocolate Ganache', 
		'Delicious vanilla cupcakes filled with a rich custard filling and topped with a decadent chocolate ganache.', 
		'1) In a small bowl, mix together the flour, salt and baking powder, set aside.
		2) In a mixing bowl fitted with a paddle attachment, cream together the sugar and butter. 
		Add the vanilla and eggs and cream together until you get a smooth mixture.
		Add the dry ingredients and with the speed on low, mix together slowly adding the milk and mix everything to combine. 
		3) Scoop the batter into your lined cupcake tin, make sure it is only filled 3/4 of the way up because they will rise. 
		Bake for 18 to 20 minutes and let cool completely before filling.
		4) Heat the cream in a small pan over medium heat until just below boiling point.
		5) Pour cream over chocolate chips and let sit for 1 minute. Add the softened butter and whisk the whole thing together until the chocolate has fully melted. Let it sit for a few minutes before frosting the cupcakes
		6) When everything is ready to be assembled, cut each cupcake horizontally, spread about 1 Tbsp of the custard on one side, 
		top with the top of the cupcake and spoon over a touch of ganache.', 
		'Dessert', 
		'Intermediate',
		12, 
		20, 
		20, 
		3
	),
	(
		'Classic Ravioli',
		'Our Classic Ravioli will take you back to nostalgic days of childhood and homemade dinners. Health Benefits: Ricotta cheese is packed with calcium.',
		'1) Make dough by mixing flour, eggs, yolks, cream, and salt. Knead until smooth. Divide and wrap in plastic.
		2) Mix ricotta, parmesan, egg yolk, salt, and pepper for filling. Refrigerate.
		3) Make marinara sauce by saut�ing onion and garlic, then adding tomatoes, oregano, red pepper, sugar, and seasoning. Simmer for 30 minutes.
		4) Roll out dough and add filling, sealing edges to make ravioli. Cut and place on lightly oiled baking sheet.
		5) Boil ravioli in salted water until they float. Serve with marinara sauce.',
		'Main Dish', 
		'Intermediate',
		12, 
		20, 
		75, 
		3
	),
	(
		'Simple White Cake',
		'Kid-friendly, minimalist, indian cake',
		'1) Preheat the oven to 350 degrees F. Grease a 9x9 inch pan or line a muffin pan with paper liners.
		2) In a bowl, cream the sugar and butter together.
		3) Crack the eggs, add them to the bowl, beat well and stir in the vanilla.
		4) Combine flour and baking powder; add to the above mixture and mix well.
		5) Finally stir in the milk and chocolate chips and mix well.
		6) Pour the batter into the prepared pan. If using muffin pan, spoon in the batter.
		7) Bake for 30 to 40 minutes in the preheated oven.
		8) For cupcakes, bake 20 to 25 minutes.
		9) The cake is done when a toothpick comes out clean when inserted in the cake and the cake springs back when touched.
		10) Remove the cake from the oven and leave aside until it becomes completely cold.
		11) Then cut into pieces and enjoy.',
		'Dessert', 
		'Easy',
		12, 
		20, 
		75, 
		3
	),
	( 
		'20-Minute Cornbread',
		'Cornbread is a quick and easy side dish that goes with pretty much anything',
		'',
		'Main Dish', 
		'Intermediate',
		4, 
		15, 
		70, 
		10
	);



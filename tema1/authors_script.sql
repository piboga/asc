use cooking_db;
go

/*
	Nickname of author (account) and email can be the same with the one of the user, since some authors can be users. I didn't use foreign keys 
	because those can only reference primary keys, and I am using IDs as primary keys
	Moreover, I decided treating the User and Author classes separately, since not all authors are users, and therefore IDs differ between
	users and authors.
*/
CREATE TABLE cooking_schema.Authors (
    id INT PRIMARY KEY IDENTITY(1,1), -- auto-incrementing primary key
    fullname VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL CHECK (email LIKE '%@%.%'),
    bio TEXT NOT NULL,
    nickname VARCHAR(255) NOT NULL, 
	experience INT NOT NULL CHECK (experience >= 0)
);

INSERT INTO cooking_schema.Authors 
(
    fullname, 
    email, 
    bio,
	nickname,
	experience
)
VALUES 
(
	'John Smith',
	'johnsmith@gmail.com',
	'John Smith is a professional chef and cookbook author. He has been cooking for over 20 years and is passionate about sharing his love of food with others.',
	'johnsmith',
	20
),
(
	'Jane Doe',
	'janedoe@gmail.com',
	'Jane Doe is a home cook and food blogger. She has a passion for creating delicious and healthy meals for her family and friends.',
	'janedoe',
	10
),
(
	'Evan Gated',
	'evgated@yahoo.com',
	'Evan Gated knows stuff. Do not question his methods',
	'evgated',
	15
),
(
	'Emily Johnson',
	'emilyj@hotmail.com',
	'Emily Johnson is a professional pastry chef and food blogger. She loves to create delicious and beautiful desserts.',
	'emilyj',
	25
),
(
	'Michael Davis',
	'mdavis@gmail.com',
	'Michael Davis is a professional chef and culinary instructor. He has a passion for teaching others about the art of cooking.',
	'mdavis',
	20
),
(
	'Samantha Lee',
	'samanthal@yahoo.com',
	'Samantha Lee is a food enthusiast and home cook. She loves to experiment with different ingredients and flavors in her kitchen.',
	'samanthal',
	5
),
(
	'Jack Smith',
	'jacksmith@gmail.com',
	'Jack Smith is a professional chef and TV host. He has been cooking for over 15 years and is passionate about sharing his love of food with others.',
	'jacksmith',
	15
),
(
	'Jasmine Lee',
	'jasminelee@yahoo.com',
	'Jasmine Lee is a home cook and food blogger. She has a passion for creating delicious and healthy meals for her family and friends.',
	'jasminelee',
	8
),
(
	'James Thompson',
	'jamest@hotmail.com',
	'James Thompson is a professional chef and culinary instructor. He has a passion for teaching others about the art of cooking.',
	'jamest',
20
),
(
	'Amy Williams',
	'amywilliams@protonmail.com',
	'Amy Williams is a professional pastry chef and food blogger. She loves to create delicious and beautiful desserts.',
	'amywilliams',
	18
),
(
	'David Brown',
	'davidbrown@gmail.com',
	'David Brown is a professional chef and cookbook author. He has been cooking for over 20 years and is passionate about sharing his love of food with others.',
	'davidbrown',
	20
),
(
	'Sophie Taylor',
	'sophie@gmail.com',
	'Sophie Taylor is a home cook and food blogger. She has a passion for creating delicious and healthy meals for her family and friends.',
	'sophie',
	7
),
(
    'Nathan Miller', 
    'nathanm@yahoo.com', 
    'Nathan Miller is a professional chef and culinary instructor. He has a passion for teaching others about the art of cooking.',
    'nathanm',
    25
),
(
    'Olivia Davis', 
    'oliviad@hotmail.com', 
    'Olivia Davis is a food enthusiast and home cook. She loves to experiment with different ingredients and flavors in her kitchen.',
    'oliviad',
    12
),
(
    'Kevin Garcia', 
    'kevingarcia@protonmail.com', 
    'Kevin Garcia is a professional chef and cookbook author. He has been cooking for over 15 years and is passionate about sharing his love of food with others.',
    'kevingarcia',
    15
),
(
    'Rachel Johnson', 
    'rachelj@gmail.com', 
    'Rachel Johnson is a professional chef and cookbook author. She has been cooking for over 20 years and is passionate about sharing her love of food with others.',
    'rachelj',
    20
),
(
    'Jacob Williams', 
    'jacobw@yahoo.com', 
    'Jacob Williams is a home cook and food blogger. He has a passion for creating delicious and healthy meals for his family and friends.',
    'jacobw',
    10
),
(
    'Nicholas Jones', 
    'nicholasj@hotmail.com', 
    'Nicholas Jones is a professional chef and culinary instructor. He has a passion for teaching others about the art of cooking.',
    'nicholasj',
    20
),
(
    'Sofia Martinez', 
    'sofiam@gmail.com', 
    'Sofia Martinez is a professional pastry chef and food blogger. She loves to create delicious and beautiful desserts.',
    'sofiam',
    18
),
(
    'Sonny Philips', 
    'sphilips@gmail.com', 
    'Sonny Phillips has been leading his italian restaurant for 15 cheerful years.',
    'sphilips',
    15
);
use cooking_db;
go
-- as foreign keys referencing an incrementing ID, recipe and ingredient ids will be limited to the range of recipes and ingredients 
CREATE TABLE cooking_schema.Recipe_Ingredient (
    recipe_id INT FOREIGN KEY REFERENCES cooking_schema.Recipes(id),
    ingredient_id INT FOREIGN KEY REFERENCES cooking_schema.Ingredients(id),
    ingredient_quantity FLOAT NOT NULL CHECK (ingredient_quantity >= 0)
);
go


INSERT INTO cooking_schema.Recipe_Ingredient (
	recipe_id, 
	ingredient_id, 
	ingredient_quantity -- while the ingredient unit of measure is the same, the quantity depends on the recipe
)
VALUES 
    (1, 1, 1), -- Spaghetti Bolognese contains ground beef, 1 pound
    (1, 2, 1), -- Spaghetti Bolognese contains spaghetti, 1 pound
    (1, 3, 1), -- Spaghetti Bolognese contains diced tomatoes, 1 can
	(2, 4, 1), -- Lava Cake contains semi-sweet chocolate chips, 1 cup
    (2, 5, 1), -- Lava Cake contains butter, 1 stick
    (2, 6, 1), -- Lava Cake contains sugar, 1 cup
    (2, 7, 4), -- Lava Cake contains eggs, 4 large
    (2, 8, 1), -- Lava Cake contains all-purpose flour, 1 cup
    (2, 9, 1), -- Lava Cake contains baking powder, 1 teaspoon
    (3, 11, 1), -- Polenta with Cheese and Sour Cream contains cornmeal, 1 cup
    (3, 12, 1), -- Polenta with Cheese and Sour Cream contains water, 1 quart
    (3, 13, 1), -- Polenta with Cheese and Sour Cream contains salt, 1 teaspoon
    (3, 14, 1), -- Polenta with Cheese and Sour Cream contains cheddar cheese, 1 cup
    (3, 15, 1), -- Polenta with Cheese and Sour Cream contains sour cream, 1 cup
	(4, 1, 1), -- Cheesy Beef and Tomato Casserole contains ground beef, 1 pound
    (4, 3, 2), -- Cheesy Beef and Tomato Casserole contains diced tomatoes, 2 cans
    (4, 14, 1), -- Cheesy Beef and Tomato Casserole contains cheddar cheese, 1 cup
    (4, 8, 1), -- Cheesy Beef and Tomato Casserole contains all-purpose flour, 1 cup
	(5, 4, 1), -- Chocolate Souffle 1 cup of semi-sweet chocolate chips
    (5, 5, 1), -- Chocolate Souffle 1 stick of butter
    (5, 6, 1), -- Chocolate Souffle 1 cup of sugar
    (5, 7, 4), -- Chocolate Souffle 4 large eggs
    (5, 8, 1), -- Chocolate Souffle 1 cup of all-purpose flour
    (5, 9, 1), -- Chocolate Souffle 1 teaspoon of baking powder
    (5, 10, 1), -- Chocolate Souffle 1 cup of milk
    (5, 14, 1), -- Chocolate Souffle 1 teaspoon of vanilla extract
    (5, 15, 1), -- Chocolate Souffle 1 cup of heavy cream
	(6, 1, 1), -- Lasagna contains 1 pound of ground beef
    (6, 2, 1), -- Lasagna contains 1 pound of spaghetti
    (6, 3, 1), -- Lasagna contains 1 can of diced tomatoes
    (6, 7, 3), -- Lasagna contains 3 large eggs
    (6, 8, 2), -- Lasagna contains 2 cups of all-purpose flour
    (6, 14, 2), -- Lasagna contains 2 cups of cheddar cheese
    (6, 15, 1), -- Lasagna contains 1 cup of sour cream
	(6, 18, 1), -- lasagna contains 1 pound of ground pork
	(6, 19, 2), -- lasagna contains 2 pounds of ricotta cheese
	(6, 20, 2), -- lasagna contains 2 pounds of mozzarella cheese
	(6, 21, 1), -- lasagna contains 1 cup of parmesan cheese
	(6, 22, 1), -- lasagna contains 1 large onion
	(6, 23, 4), -- lasagna contains 4 cloves of garlic
	(6, 24, 1), -- lasagna contains 1 teaspoon of oregano
	(6, 25, 1), -- lasagna contains 1 teaspoon of basil
	(6, 26, 1), -- lasagna contains 1 teaspoon of nutmeg
	(6, 27, 2), -- lasagna contains 2 tablespoons of olive oil
	(6, 28, 1), -- lasagna contains 1 teaspoon of red pepper flakes
	(6, 29, 1), -- lasagna contains 1 pack of no-boil lasagna noodles
	(7, 5, 1/2), -- Vanilla Cupcakes with Custard Filling contains 1/2 cup of unsalted butter
    (7, 6, 2/3), -- Vanilla Cupcakes with Custard Filling contains 2/3 cup of granulated sugar
    (7, 7, 2), -- Vanilla Cupcakes with Custard Filling contains 2 eggs
    (7, 9, 2), -- Vanilla Cupcakes with Custard Filling contains 2 teaspoons of baking powder
    (7, 8, 1/2), -- Vanilla Cupcakes with Custard Filling contains 1/2 cups of all-purpose flour
    (7, 10, 1/2), -- Vanilla Cupcakes with Custard Filling contains 1/2 cup of whole milk
    (7, 13, 1/8), -- Vanilla Cupcakes with Custard Filling contains 1/8 teaspoon of salt
    (7, 16, 1), -- Vanilla Cupcakes with Custard Filling contains 1 teaspoon of vanilla past
	(8, 8, 2), -- Classic Ravioli contains 2 cups of all-purpose flour
	(8, 7, 3), -- ...
	(8, 13, 1),
	(8, 14, 1),
	(8, 7, 3),
	(8, 17, 2),
	(8, 19, 1),
	(8, 21, 0.25),
	(8, 27, 0.25),
	(8, 22, 1),
	(8, 23, 4),
	(8, 3, 28),
	(8, 24, 2),
	(8, 6, 0.5),
	(8, 28, 1),
	(8, 25, 0.25),
	(9, 8, 1.5), -- Simple White Cake contains 1.5 cups of all-purpose flour
	(9, 10, 0.5), 
	(9, 9, 1.75),
	(9, 6, 0.60),
	(9, 5, 0.5),
	(9, 4, 2),
	(9, 14, 2),
	(9, 4, 1),
	(10, 8, 1), -- 20-Minute Cornbrea contains 1 cup of all-purpose flour
	(10, 11, 0.75),
	(10, 6, 2),
	(10, 13, 0.75),
	(10, 5, 1),
	(10, 7, 2),
	(10, 10, 1),
	(10, 30, 0.75);
go
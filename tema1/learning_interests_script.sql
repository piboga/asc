use cooking_db;
go

CREATE TABLE cooking_schema.Learning_Interests (
	id INT PRIMARY KEY IDENTITY(1,1),
	title NVARCHAR(50) NOT NULL,
	skill_score INT NOT NULL CHECK (skill_score BETWEEN 1 AND 5)
);

INSERT INTO cooking_schema.Learning_Interests (title, skill_score)
VALUES
	('Baking', 2),
	('Grilling', 1),
	('Sous vide cooking', 3),
	('Fermentation', 3),
	('Molecular gastronomy', 5),
	('Vegetarian and vegan cooking', 2),
	('Farm-to-table cooking', 3),
	('Global cuisine', 3),
	('Pastry and dessert making', 4),
	('Slow cooking and braising', 2),
	('Seafood and fish preparation', 3),
	('Barbecue and smoking', 3),
	('Charcuterie and cured meats', 4),
	('Cheese making', 4),
	('Culinary history and traditional techniques', 3);
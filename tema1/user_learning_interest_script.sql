use cooking_db;
go

CREATE TABLE cooking_schema.User_Learning_Interest (
	learning_user_id INT FOREIGN KEY REFERENCES cooking_schema.Users(id),
	learning_interest_id INT FOREIGN KEY REFERENCES cooking_schema.Learning_Interests(id)  
);

INSERT INTO cooking_schema.User_Learning_Interest (
	learning_user_id,
	learning_interest_id
)
VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 6),
	(2, 2),
	(2, 4),
	(2, 6),
	(2, 7),
    (3, 3),
    (3, 5),
    (3, 6),
    (4, 4),
    (4, 5),
    (4, 6),
    (5, 1),
    (5, 4),
    (5, 5),
    (5, 12),
    (5, 13),
    (6, 6),
    (6, 12),
    (6, 13),
    (7, 7),
    (7, 8),
    (7, 9),
    (8, 8),
    (8, 9),
    (8, 10),
    (9, 9),
    (9, 10),
    (9, 11),
    (10, 10),
    (10, 11),
    (10, 12),
    (11, 11),
    (11, 12),
    (11, 13),
    (12, 12),
    (12, 13),
    (13, 13),
    (14, 7),
    (14, 8),
    (14, 9),
    (14, 10),
    (15, 8),
    (15, 9),
    (15, 10),
    (16, 9),
    (16, 10),
    (16, 11),
    (17, 10),
    (17, 11),
    (17, 12),
    (18, 1),
    (18, 3),
    (18, 5),
    (18, 10),
    (18, 15),
	(19, 1),
	(20, 2);
 go
"""
This module represents the Marketplace.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

class Marketplace:
    """
    Class that represents the Marketplace. It's the central part of the implementation.
    The producers and consumers use its methods concurrently.
    """
    def __init__(self, queue_size_per_producer):
        """
        Constructor

        :type queue_size_per_producer: Int
        :param queue_size_per_producer: the maximum size of a queue associated with each producer
        """

        self.queue_size_per_producer = queue_size_per_producer
        self.producers = [] # list of producers' queues
        self.consumers = [] # list of consumers' carts
        self.product_to_producer = {} # dictionary that maps products to their producer

    def is_full(self, producer_id):
        """
        Return True if the producer's queue is full.

        :type producer_id: Int
        :param producer_id: id producer
        """
        return len(self.producers[producer_id]) == self.queue_size_per_producer


    def register_producer(self):
        """
        Returns an id for the producer that calls this.
        """

        producer_id = len(self.producers) # by nature, the ID of the producer will be the its position in the list
        self.producers.append([])
        return producer_id

    def publish(self, producer_id, product):
        """
        Adds the product provided by the producer to the marketplace

        :type producer_id: String
        :param producer_id: producer id

        :type product: Product
        :param product: the Product that will be published in the Marketplace

        :returns True or False. If the caller receives False, it should wait and then try again.
        """

        if not self.is_full(producer_id):
            self.producers[producer_id].append(product) # add the product to the producer's queue
            self.product_to_producer[product] = producer_id # remember the producer of the product
            return True
        return False

    def new_cart(self):
        """
        Creates a new cart for the consumer

        :returns an int representing the cart_id
        """
        cart_id = len(self.consumers) # by nature, the ID of the cart will be the its position in the list
        self.consumers.append([])
        return cart_id

    def add_to_cart(self, cart_id, product):
        """
        Adds a product to the given cart. The method returns

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to add to cart

        :returns True or False. If the caller receives False, it should wait and then try again
        """
        # Check if id is valid
        if cart_id < len(self.consumers):
            producer_id = 0
            while producer_id < len(self.producers):
                producer = self.producers[producer_id]
                # Check if product is in a producer's queue and if it is, remove it and add it to the cart
                if product in producer:
                    producer.remove(product)
                    self.consumers[cart_id].append(product)
                    return True
                producer_id += 1
        return False

    def remove_from_cart(self, cart_id, product):
        """
        Removes a product from cart.

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to remove from cart
        """

        # Check if the cart id is valid, the product is in the cart 
        # and the queue can still hold elements
        is_inside = product in self.consumers[cart_id]
        can_hold = not self.is_full(self.product_to_producer[product])
        if cart_id < len(self.consumers) and is_inside and can_hold:
            self.consumers[cart_id].remove(product)
            self.producers[self.product_to_producer[product]].append(product)
            return True
        return False

    def place_order(self, cart_id):
        """
        Return a list with all the products in the cart.

        :type cart_id: Int
        :param cart_id: id cart
        """
        consumer = self.consumers[cart_id] # get the cart to be emptied and returned
        self.consumers[cart_id] = []
        return consumer

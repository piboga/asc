"""
This module represents the Producer.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

from threading import Thread
from time import sleep


class Producer(Thread):
    """
    Class that represents a producer.
    """

    def __init__(self, products, marketplace, republish_wait_time, **kwargs):
        """
        Constructor.

        @type products: List()
        @param products: a list of products that the producer will produce

        @type marketplace: Marketplace
        @param marketplace: a reference to the marketplace

        @type republish_wait_time: Time
        @param republish_wait_time: the number of seconds that a producer must
        wait until the marketplace becomes available

        @type kwargs:
        @param kwargs: other arguments that are passed to the Thread's __init__()
        """
        Thread.__init__(self, **kwargs)
        self.products = products
        self.marketplace = marketplace
        self.republish_wait_time = republish_wait_time
        self.producer_id = self.marketplace.register_producer()

    def run(self):
        while True:
            # traverse each product
            product_id = 0
            while product_id < len(self.products):
                product = self.products[product_id]
                quantity = product[1]
                post_production_time = product[2]
                # produce as much as quantity
                while quantity > 0:
                    # try to publish the product, if it fails, retry after a while
                    while True:
                        if self.marketplace.publish(self.producer_id, product[0]):
                            sleep(post_production_time)
                            break
                        sleep(self.republish_wait_time)
                    quantity -= 1
                product_id += 1

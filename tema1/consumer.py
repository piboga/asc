"""
This module represents the Consumer.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

from threading import Thread
from time import sleep

class Consumer(Thread):
    """
    Class that represents a consumer.
    """

    def __init__(self, carts, marketplace, retry_wait_time, **kwargs):
        """
        Constructor.

        :type carts: List
        :param carts: a list of add and remove operations

        :type marketplace: Marketplace
        :param marketplace: a reference to the marketplace

        :type retry_wait_time: Time
        :param retry_wait_time: the number of seconds that a producer must wait
        until the Marketplace becomes available

        :type kwargs:
        :param kwargs: other arguments that are passed to the Thread's __init__()
        """
        Thread.__init__(self, **kwargs)
        self.carts = carts
        self.marketplace = marketplace
        self.retry_wait_time = retry_wait_time

    def run(self):
        # Traverse each cart (each list of operations)
        id_ops = 0
        while id_ops < len(self.carts):
            operations = self.carts[id_ops]
            cart_id = self.marketplace.new_cart()

            # Traverse each operation in the cart
            id_op = 0
            while id_op < len(operations):
                operation = operations[id_op]

                # get the function to be called for the quantity
                function = self.marketplace.add_to_cart if operation["type"] == "add" else self.marketplace.remove_from_cart

                remaining = operation["quantity"]
                while remaining > 0:
                    # try to execute the adding / removal, if it fails, retry after a while
                    while True:
                        if function(cart_id, operation["product"]) == True:
                            break
                        else:
                            sleep(self.retry_wait_time)
                    remaining -= 1
                id_op += 1

            # empty the cart and print its contents
            checkout = self.marketplace.place_order(cart_id)
            id_ops += 1
            for product in checkout:
                print("{} bought {}".format(self.name, product))
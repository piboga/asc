/*
 * Tema 2 ASC
 * 2022 Spring
 */
#include "utils.h"

double* my_solver(int N, double *A, double* B) {

	// Calculate Bt * B
	register int i, j, k;

	double *Bt_B = (double*)calloc(N * N, sizeof(double));
	for (i = 0; i < N; ++i) {
		double* Bt_B_i_j = &Bt_B[i * N];
		
		for (j = 0; j < N; ++j) {
			register double sum = 0;
			register double* B_k_i = &B[i]; // B[0 * N + i]
			register double* B_k_j = &B[j]; // B[0 * N + j]

			for (k = 0; k < N; k += 4) {
				sum += *B_k_i * *B_k_j + *(B_k_i + N)* *(B_k_j + N) + *(B_k_i + 2 * N) * *(B_k_j + 2 * N) + *(B_k_i + 3 * N) * *(B_k_j + 3 * N);
				B_k_i += N;
				B_k_j += N;
			}
			*Bt_B_i_j = sum;
			++Bt_B_i_j;
		}
	}

	// multiplicate upper triangular A with lower triangular A transpose
	// A[i][k] = 0 if i >= k and A[j][k] = 0 if j >= k, ignore those values

	register double *A_At = (double*)calloc(N * N, sizeof(double));
	register int start;

	for (i = 0; i < N; ++i) {
		register double* A_At_i_j = &A_At[i * N];
		for (j = 0; j < N; ++j) {
			register double sum = 0;
			start = i < j ? i : j;
			
			register double* A_i_k = &A[i * N + start];
			register double* A_j_k = &A[j * N + start];

			for (k = start; k < N; ++k) {
				sum += *A_i_k * *A_j_k;
				++A_i_k;
				++A_j_k;
			}
			*A_At_i_j = sum;
			++A_At_i_j;
		}
	}
	// C=B*A*At+Bt*B
	register double *C = (double*)calloc(N * N, sizeof(double));
	
	register double* Bt_B_i_j = &Bt_B[0];
	register double* C_i_j = &C[0];

	for (i = 0; i < N; ++i) {
		for (j = 0; j < N; ++j) {
			register double sum = 0;

			register double* B_i_k = &B[i * N];
			register double* A_At_k_j = &A_At[j]; // A_At[0 * N + j]

			for (k = 0; k < N; k += 4) {
				sum += *B_i_k * *A_At_k_j + *(B_i_k + 1) * *(A_At_k_j + N) + *(B_i_k + 2) * *(A_At_k_j + 2 * N) + *(B_i_k + 3) * *(A_At_k_j + 3 * N);
				++B_i_k;
				A_At_k_j += N;
			}
			*C_i_j = sum + *Bt_B_i_j;
			++Bt_B_i_j;
			++C_i_j;
		}
	}

	// Free memory
	free(Bt_B);
	free(A_At);
	
	return C;
}


void print_matrix(double* A, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			printf("%f ", A[i * n + j]);
		}
		printf("\n");
	}
}

int main()
{
	// initialize double * A malloc matrix
	int N = 4;
	double *A = (double*)calloc(N * N, sizeof(double));
	double *B = (double*)calloc(N * N, sizeof(double));
	
	// populate B
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			B[i * N + j] = i * N + j + 1;
		}
	}
	
	// Create A, an upper triangular matrix
	for (int i = 0; i < N; ++i) {
		for (int j = i; j < N; ++j) {
			A[i * N + j] = i * N + j + 1;
		}
	}

	// my solver
	double* C = my_solver(N, A, B);

	// print A
	printf("A = \n");
	print_matrix(A, N);
	printf("\n");
	// print B
	printf("B = \n");
	print_matrix(B, N);
	printf("\n");
	// print C
	printf("C = \n");
	print_matrix(C, N);
	printf("\n");

	return 0;
}

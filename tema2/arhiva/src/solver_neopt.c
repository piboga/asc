/*
 * Tema 2 ASC
 * 2022 Spring
 */
#include "utils.h"

/*
 * Add your unoptimized implementation here
 */

double* my_solver(int N, double *A, double* B) {

	// Calculate Bt * B
	double *Bt_B = (double*)calloc(N * N, sizeof(double));

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			for (int k = 0; k < N; ++k) {
				Bt_B[i * N + j] += B[k * N + i] * B[k * N + j];
			}
		}
	}

	// multiplicate upper triangular A with lower triangular A transpose
	// A[i][k] = 0 if i >= k and A[j][k] = 0 if j >= k, ignore those values
	double *A_At = (double*)calloc(N * N, sizeof(double));
	for (int k = 0; k < N; ++k) {
		for (int i = 0; i <= k; ++i) {
			for (int j = 0; j <= k; ++j) {
					A_At[i * N + j] += A[i * N + k] * A[j * N + k];
			}
		}
	}

	// C=B*A*At+Bt*B
	double *C = (double*)calloc(N * N, sizeof(double));
	
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			for (int k = 0; k < N; ++k) {
				C[i * N + j] += B[i * N + k] * A_At[k * N + j];
			}
			C[i * N + j] +=  + Bt_B[i * N + j];
		}
	}

	// Free memory
	free(Bt_B);
	free(A_At);
	
	return C;
}

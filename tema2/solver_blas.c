/*
 * Tema 2 ASC
 * 2022 Spring
 */
#include "utils.h"
#include "cblas.h"
#include <string.h> // Memcpy

/* 
 * Add your BLAS implementation here
 */

double* my_solver(int N, double *A, double *B) {
	// Calculate Bt * B

	double *Bt_B = (double*)calloc(N * N, sizeof(double));
	// Z <- aXY + bZ
	// Let a = 1, b = 0, X = Bt, Y = B, Z = Bt_B
	cblas_dgemm(
		CblasRowMajor, // C-style order
		CblasTrans, // 1st matrix is Bt
		CblasNoTrans, // 2nd matrix is B
		N, // rows of 1st matrix, Bt
		N, // cols of 2nd matrix, B
		N, // cols of 1st matrix, Bt
		1.0, // alpha
		B, // pointer to 1st matrix - Bt
		N, // size of 1st matrix, Bt
		B, // pointer to 2nd matrix - B
		N, // size of 2nd matrix, B
		0.0, // beta
		Bt_B, // pointer to 3rd matrix
		N // size of 3rd matrix
	);

	// Calculate A * At
	// Y <- a * Y * X' 
	// Let a = 1, Y = X = A
	// Since Y will be overwritten with A_At, we copy A to A_At
	double *A_At = (double*)calloc(N * N, sizeof(double));
	memcpy(A_At, A, N * N * sizeof(*A_At));
	cblas_dtrmm(
			CblasRowMajor, // C-style matrix order
			CblasRight, // multiply from right to left, since we want to transpose the second matrix
			CblasUpper, // X = A is upper triangular
			CblasTrans, // X = A is transposed
			CblasNonUnit, // X = A is not unit triangular
			N,
			N,
			1.0, // alpha
			A,
			N,
			A_At,
			N
	);

	// C = B * A_At + Bt_B
	// Calculate B * A_At
	double *B_A_At = (double*)calloc(N * N, sizeof(double));
	cblas_dgemm(
		CblasRowMajor, // C-style order
		CblasNoTrans, // 1st matrix is B
		CblasTrans, // 2nd matrix is A_At
		N, // rows of 1st matrix, B
		N, // cols of 2nd matrix, A_At
		N, // cols of 1st matrix, B
		1.0, // alpha
		B, // pointer to 1st matrix - B
		N, // size of 1st matrix, B
		A_At, // pointer to 2nd matrix - A_At
		N, // size of 2nd matrix, A_At
		0.0, // beta
		B_A_At, // pointer to 3rd matrix
		N // size of 3rd matrix
	);

	// Add B_A_At to Bt_B to get C
	// Since matrixes are represented as vectors, we can use daxpy
	// (a * X[i]) + Y[i]
	cblas_daxpy(
		N * N, // size of C
		1.0, // alpha
		B_A_At, // pointer to 1st, B_A_At
		1, // stride of B_A_At, every (1) element is considered
		Bt_B, // pointer to 2nd, Bt_B
		1 // stride of Bt_B, every element is considered
	);

	// Copy overwritten Bt_B to C
	double *C = (double*)calloc(N * N, sizeof(double));
	memcpy(C, Bt_B, N * N * sizeof(*C));

	// Free memory
	free(B_A_At);
	free(A_At);
	free(Bt_B);

	return C;
}

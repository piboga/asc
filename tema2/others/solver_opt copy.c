/*
 * Tema 2 ASC
 * 2022 Spring
 */
#include "utils.h"

/*
 * Add your unoptimized implementation here
 */

void print_matrix(double* A, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			printf("%f ", A[i * n + j]);
		}
		printf("\n");
	}
}

double* my_solver(int N, double *A, double* B) {

	/* 
		Calculate Bt * B
	*/
	

	// "Configuratiile k-i-j si i-k-j ar trebui sa aiba cele mai bune performante"
	double *Bt_B = (double*)calloc(N * N, sizeof(double));
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			register double sum = 0.0F;
			for (int k = 0; k < N; ++k) {
				sum += B[k * N + i] * B[k * N + j];
			}
			Bt_B[i * N + j] = sum;
		}
	}

	// multiplicate upper triangular A with lower triangular A transpose
	// A[i][k] = 0 if i >= k and A[j][k] = 0 if j >= k, ignore those values
	double *A_At = (double*)calloc(N * N, sizeof(double));
	for (int k = 0; k < N; ++k) {
		for (int i = 0; i <= k; ++i) {
			for (int j = 0; j <= k; ++j) {
				A_At[i * N + j] += A[i * N + k] * A[j * N + k];
			}
		}
	}

	// C=B*A*At+Bt*B
	double *C = (double*)calloc(N * N, sizeof(double));
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			register double sum = 0.0F;
			for (int k = 0; k < N; ++k) {
				sum += B[i * N + k] * A_At[k * N + j];
			}
			C[i * N + j] = sum + Bt_B[i * N + j];
		}
	}

	// Free memory
	free(Bt_B);
	free(A_At);
	
	return C;
}



int main()
{
	// initialize double * A malloc matrix
	int N = 3;
	double *A = (double*)calloc(N * N, sizeof(double));
	double *B = (double*)calloc(N * N, sizeof(double));
	
	// populate B
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			B[i * N + j] = i * N + j + 1;
		}
	}
	
	// Create A, an upper triangular matrix
	for (int i = 0; i < N; ++i) {
		for (int j = i; j < N; ++j) {
			A[i * N + j] = i * N + j + 1;
		}
	}

	// my solver
	double* C = my_solver(N, A, B);

	// print A
	printf("A = \n");
	print_matrix(A, N);
	printf("\n");
	// print B
	printf("B = \n");
	print_matrix(B, N);
	printf("\n");
	// print C
	printf("C = \n");
	print_matrix(C, N);
	printf("\n");

	return 0;
}

/*
 * Tema 2 ASC
 * 2022 Spring
 */
#include "utils.h"

/*
 * Add your unoptimized implementation here
 */

void print_matrix(double* A, int n) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			printf("%f ", A[i * n + j]);
		}
		printf("\n");
	}
}

double* my_solver(int N, double *A, double* B) {

	/* 
		Calculate Bt * B
	*/
	

	// "Configuratiile k-i-j si i-k-j ar trebui sa aiba cele mai bune performante"
	double *Bt_B = (double*)calloc(N * N, sizeof(double));
	for (int i = 0; i < N; ++i) {
		double* Bt_B_i_j = &Bt_B[i * N];
		
		for (int j = 0; j < N; ++j) {
			register double sum = 0.0F;
			double* B_k_i = &B[i]; // B[0 * N + i]
			double* B_k_j = &B[j]; // B[0 * N + j]

			for (int k = 0; k < N; ++k) {
				sum += *B_k_i * *B_k_j;
				B_k_i += N;
				B_k_j += N;
			}
			*Bt_B_i_j = sum;
			++Bt_B_i_j;
		}
	}

	// multiplicate upper triangular A with lower triangular A transpose
	// A[i][k] = 0 if i >= k and A[j][k] = 0 if j >= k, ignore those values
	double *A_At = (double*)calloc(N * N, sizeof(double));
	for (int k = 0; k < N; ++k) {
		double *A_i_k = &A[k]; // A[0 * N + k]
		
		for (int i = 0; i <= k; ++i) {
			double *A_At_i_j = &A_At[i * N];
			double *A_j_k = &A[k]; // A[0 * N + k]

			for (int j = 0; j <= k; ++j) {
				*A_At_i_j += *A_i_k  * *A_j_k;
				++A_At_i_j;
				A_j_k += N;
			}
			A_i_k += N;
		}

	}

	// C=B*A*At+Bt*B
	double *C = (double*)calloc(N * N, sizeof(double));
	
	double* Bt_B_i_j = &Bt_B[0];
	double* C_i_j = &C[0];

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			register double sum = 0.0F;

			double* B_i_k = &B[i * N];
			double* A_At_k_j = &A_At[j]; // A_At[0 * N + j]

			for (int k = 0; k < N; ++k) {
				sum += *B_i_k * *A_At_k_j;
				++B_i_k;
				A_At_k_j += N;
			}
			*C_i_j = sum + *Bt_B_i_j;
			++Bt_B_i_j;
			++C_i_j;
		}
	}

	// Free memory
	free(Bt_B);
	free(A_At);
	
	return C;
}



int main()
{
	// initialize double * A malloc matrix
	int N = 3;
	double *A = (double*)calloc(N * N, sizeof(double));
	double *B = (double*)calloc(N * N, sizeof(double));
	
	// populate B
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			B[i * N + j] = i * N + j + 1;
		}
	}
	
	// Create A, an upper triangular matrix
	for (int i = 0; i < N; ++i) {
		for (int j = i; j < N; ++j) {
			A[i * N + j] = i * N + j + 1;
		}
	}

	// my solver
	double* C = my_solver(N, A, B);

	// print A
	printf("A = \n");
	print_matrix(A, N);
	printf("\n");
	// print B
	printf("B = \n");
	print_matrix(B, N);
	printf("\n");
	// print C
	printf("C = \n");
	print_matrix(C, N);
	printf("\n");

	return 0;
}
